<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/book', 'BookController@index')->name('book.index');
Route::get('/book/show/{id}', 'BookController@show')->name('book.show');
Route::get('/book/edit/{id}', 'BookController@edit')->name('book.edit');
Route::post('/book/update/{id}', 'BookController@update')->name('book.update');
Route::get('/book/{id}','BookController@delete')->name('book.delete');
Route::get('create', 'BookController@create')->name('create');
Route::post('/book/store', 'BookController@store')->name('book.store');
Route::post('/book/comment/store/{id}', 'BookController@commentStore')->name('comment.store');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('profile', function(){
    //Only authenticated users may enter...
  })->middleware('auth');
