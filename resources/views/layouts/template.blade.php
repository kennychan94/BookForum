<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Book Forum') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
	<div id="app">
		<nav class="navbar navbar-default navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				{{--  <a class="navbar-brand" href="#">Laravel</a>  --}}
			</div>
            
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                
				{{-- <ul class="nav navbar-nav">
                    @if(Auth::user()->role == "author")
					<li class="{{ (Request::is('/') ? 'active' : '') }}">
						<a href="{{ url('') }}"><i class="fa fa-home"></i> Home</a>
					</li>
					<li class="{{ (Request::is('book') ? 'active' : '') }}">
						<a href="{{ url('book') }}">View Books</a>
					</li>
					<li class="{{ (Request::is('create') ? 'active' : '') }}">
						<a href="{{ url('create') }}">Upload Book</a>
                    </li>
                    @else
                    <li class="{{ (Request::is('/') ? 'active' : '') }}">
						<a href="{{ url('') }}"><i class="fa fa-home"></i> Home</a>
					</li>
					<li class="{{ (Request::is('book') ? 'active' : '') }}">
						<a href="{{ url('book') }}">View Books</a>
                    </li>
                    @endif
                </ul> --}}
                
                <ul class="nav navbar-nav"> 
                        <li class="{{ (Request::is('/') ? 'active' : '') }}">
                            <a href="{{ url('') }}"><i class="fa fa-home"></i> Home</a>
                        </li>
                        <li class="{{ (Request::is('book') ? 'active' : '') }}">
                            <a href="{{ url('book') }}">View Books</a>
                        </li>
                        <li class="{{ (Request::is('create') ? 'active' : '') }}">
                            <a href="{{ url('create') }}">Publish Book</a>
                        </li>
                    </ul>
	
				<!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
			</div>
		</div>
		</nav>
		@yield('content')
	</div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>