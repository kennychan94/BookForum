<?php

use App\Common;

?>
@extends('layouts.template')

@section('content')
  <!-- Bootstrap Boilerplate... -->
  <div class="panel-body">
    @if($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    <!-- New Book Form -->
    {!! Form::model($book, [
        'route' => ['book.store'],
        'class' => 'form-horizontal'
    ]) !!}

    <!-- Title -->
    <div class="form-group row">
        {!! Form::label('book-title', 'Title', [
        'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
            {!! Form::text('title', $book->code, [
                'id' => 'book-title',
                'class' => 'form-control',
            ]) !!}
        </div>
    </div>

    <!-- Summary -->
    <div class="form-group row">
        {!! Form::label('book-summary', 'Summary', [
            'class' => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
            {!! Form::text('summary', $book->summary, [
                'id' => 'book-summary',
                'class' => 'form-control',
                'maxlength' => 100,
            ]) !!}
        </div>
    </div>

      <!-- Category -->
      <div class="form-group row">
        {!! Form::label('book-category', 'Category', [
          'class'         => 'control-label col-sm-3',
        ]) !!}
        <div class="col-sm-9">
          {!! Form::select('category_id', Common::$categories, null, [
            'class'       => 'form-control',
            'placeholder' => '- Select Category -',
          ]) !!}
        </div>
      </div>

      <!-- Submit Button -->
      <div class="form-group row">
        <div class="col-sm-offset-3 col-sm-6">
          {!! Form::button('Save', [
            'type'        => 'submit',
            'class'       => 'btn btn-primary',
          ]) !!}
        </div>
      </div>
    {!! Form::close() !!}
  </div>

@endsection
