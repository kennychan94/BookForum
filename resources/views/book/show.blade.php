@extends('layouts.template')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="row">
            <div class="box">
                <div class="col-lg-12 text-center">
                    <h2>{{ $book->title }}
                    <br>
                    <small>by {{ $book->user->name }} on the {!! $book->updated_at !!}</small>
                    </h2>
                    @if ($book->category_id == 1)
                    <p>This is a Sci-fi book.</p>
                    @elseif ($book->category_id == 2)
                    <p>This is a Fiction book.</p>
                    @elseif ($book->category_id == 3)
                    <p>This is a Comic book.</p>
                    @else ($book->category_id == 4)
                    <p>This is a Novel.</p>
                    @endif
                    <p>{!! $book->summary !!}</p>
                    {{-- <p>{{  $book->category_id }}</p> --}}
                            {{-- {!! link_to_route(
                                'book.show', 
                                $title = 'Show', 
                                $parameters = 
                                ['id' => $book->id,
                                ]) 
                            !!}
                            &nbsp
                            &nbsp
                            {!! link_to_route(
                                'book.edit',
                                $title = 'Edit',
                                $parameters = [
                                'id' => $book->id,
                                ]) 
                            !!}
                            &nbsp
                            &nbsp
                            {!! link_to_route(
                                'book.delete',
                                $title = 'Delete',
                                $parameters = [
                                'id' => $book->id,
                                ]) 
                            !!} --}}
                            {{-- {!! link_to_route(
                                'book.comment',
                                $title = 'Comment',
                                $parameters = [
                                'id' => $book->id,
                                ]) 
                            !!} --}}
                </div>
            </div>
        </div>
        <br>
        <div class="row">
                <div class="box">
                    <div class="col-lg-12">
                        <div class="col-lg-12">
                                
                            <h4 class="text-center">COMMENTS</h4>
                            <hr>
        
                            {{-- @if($comments->count()) --}}
                            @if(count($comments))
                            {{-- dd($comments); --}}
                                @foreach($comments as $comment)
                                    <div class="col-lg-12 text-center">
                                            {{-- <h2>{{ $book->title }}
                                                    <br>
                                                    <small>by {{ $book->user->name }} on the {!! $book->updated_at !!}</small>
                                            </h2> --}}
                                            {{-- <p>{{ $comment->content }}</p> --}}
                                        <h3>
                                            <small> by {!! $comment->user->name!!} on {!! $comment->created_at !!}</small>
                                        </h3>
                                        <div>{!! $comment->content !!}</div>
                                        <hr>
                                    </div>
                                @endforeach
                            @endif	
        
                            {{-- <div class="row" id="formcreate"> 
                                    {!! Form::open(['url' => 'comment.store']) !!}	
                                        {!! Form::hidden('book_id', $book->id) !!}
                                        {!! Form::control('textarea', 12, 'comments') !!}
                                        {!! Form::submit() !!}
                                    {!! Form::close() !!}
                                @else
                                    <div class="text-center"><i class="text-center">{{ trans('front/blog.info-comment') }}</i></div>
                                @endif
                            </div> --}}
                            <div class="text-center">
                                    {{ Form::open(array('url'=>'/book/comment/store/'. $book->id, 'method' => 'post' , 'class' => 'form-inline' )) }}        
                                      <div class="form-group" style="width:100%; position:relative">                             
                                          {{ Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => 'Add your comment', 'rows' => '4']) }}
                                      </div>
                                      <div class="form-group"> 
                                        {{ Form::hidden('id', $book->id) }}  
                                        <br>
                                        {{ Form::submit('Post Comment', array('class' => 'btn btn-block btn-primary' , 'style' => 'width:220px')) }}
                                      </div>
                                    {{ Form::close() }}         
                            </div>
        
                        </div>
                    </div>
                </div>
            </div>

@endsection