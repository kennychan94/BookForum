@extends('layouts.template')

@section('content')


    @foreach($books as $book)
    <div class="row">
        <div class="box">
            <div class="col-lg-12 text-center">
                <h2>{{ $book->title }}
                <br>
                <small>by {{ $book->user->name }} on the {!! $book->updated_at !!}</small>
                </h2>
                
                @if ($book->category_id == 1)
                <p>This is a Sci-fi book.</p>
                @elseif ($book->category_id == 2)
                <p>This is a Fiction book.</p>
                @elseif ($book->category_id == 3)
                <p>This is a Comic book.</p>
                @else ($book->category_id == 4)
                <p>This is a Novel.</p>
                @endif
                
                <p>{!! $book->summary !!}</p>
                        <a class="btn btn-primary" href="{{route('book.show', $book->id)}}">Show</a>
                        <a class="btn btn-info" href="{{route('book.edit', $book->id)}}">Edit</a>
                        <a class="btn btn-danger" onclick="return confirm('Are you sure?')" href="{{route('book.delete', $book->id)}}">Delete</a>
            </div>
        </div>
    </div>
    @endforeach

{{-- <div class="panel-body">
                @if (count($books) > 0)
                    <table class="table table-striped task-table">
                    <!-- Table Headings -->
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>Author Name</th>
                                <th>Publish Time</th>
                                <th>Summary</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <!-- Table Body -->
                        <tbody>
                            @foreach ($books as $i => $book)
                                <tr>
                                    <td class="table-text">
                                        <div>{{ $i+1 }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $book->title }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $book->user->name }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $book->created_at }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $book->summary }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>
                                            {!! link_to_route(
                                                'book.show', 
                                                $title = 'Show', 
                                                $parameters = 
                                                ['id' => $book->id,
                                                ]) 
                                            !!}
                                            &nbsp
                                            &nbsp
                                    
                                            {!! link_to_route(
                                                'book.index',
                                                $title = 'Edit',
                                                $parameters = [
                                                'id' => $book->id,
                                                ]) 
                                            !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <div>
                        no records found
                        </div>
                @endif
</div> --}}
@endsection