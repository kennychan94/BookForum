{{--  @extends('layouts.app')  --}}
@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                <div class="row">
                        <div class="col-lg-12 text-center">
                            <h3> This is a book forum where authors can view books, publish books, edit books details, comment on the books and delete books! 
                                While for the readers, you can view books details and comment on the books! </h3>
                                <br>
                                <br>
                                <h4>Have Fun!</h4>
                                <br>
                                <h6>A small reminder! Please login or register yourself before having fun with this system!</h6>
                        </div>
                    </div>
        </div>
    </div>
</div>
@endsection
