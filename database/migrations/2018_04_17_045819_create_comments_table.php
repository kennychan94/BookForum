<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
			$table->integer('user_id')->unsigned();
			$table->integer('book_id')->unsigned();
            $table->timestamps();
        });

        DB::table('comments')->insert([
            ['id'=>'1', 'content'=>'comment1', 'user_id'=>'3', 'book_id'=>'1', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'2', 'content'=>'comment2', 'user_id'=>'3', 'book_id'=>'1', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'3', 'content'=>'comment3', 'user_id'=>'3', 'book_id'=>'2', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'4', 'content'=>'comment4', 'user_id'=>'3', 'book_id'=>'3', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
