<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class InitRolesAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Define roles
        $author = Bouncer::role()->create([
            'name' => 'author',
            'title' => 'Author',
        ]);
            
        $reader = Bouncer::role()->create([
            'name' => 'reader',
            'title' => 'Reader',
        ]);

        // Define abilities
        $viewBook = Bouncer::ability()->create([
            'name' => 'view-book',
            'title' => 'View Book',
        ]);

        $createBook = Bouncer::ability()->create([
            'name' => 'create-book',
            'title' => 'Create Book',
        ]);

        $manageBook = Bouncer::ability()->create([
            'name' => 'manage-book',
            'title' => 'Manage Book'
        ]);

        $commentBook = Bouncer::ability()->create([
            'name' => 'comment-book',
            'title' => 'Comment Book',
        ]);

        $deleteBook = Bouncer::ability()->create([
            'name' => 'delete-book',
            'title' => 'Delete Book',
        ]);

        // Assign abilities to roles
        Bouncer::allow($author)->to($viewBook);
        Bouncer::allow($author)->to($createBook);
        Bouncer::allow($author)->to($manageBook);
        Bouncer::allow($author)->to($commentBook);
        Bouncer::allow($author)->to($deleteBook);

        Bouncer::allow($reader)->to($viewBook);
        Bouncer::allow($reader)->to($commentBook);

        // Make the first user an admin
        $user = User::find(1);
        Bouncer::assign('author')->to($user);
        $user = User::find(3);
        Bouncer::assign('reader')->to($user);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
