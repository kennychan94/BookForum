<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    
        DB::table('categories')->insert([
            ['id'=>'1', 'name'=>'sci-fi', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'2', 'name'=>'fiction', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'3', 'name'=>'comic', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'4', 'name'=>'novel', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
