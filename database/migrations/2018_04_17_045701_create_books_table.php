<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('summary');
            $table->integer('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        DB::table('books')->insert([
            ['id' => '1', 'title' => 'Book1', 'summary' => 'This is book 1.', 'category_id' => '1', 'user_id' => '1', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id' => '2', 'title' => 'Book2', 'summary' => 'This is book 2.', 'category_id' => '1', 'user_id' => '1', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id' => '3', 'title' => 'Book3', 'summary' => 'This is book 3.', 'category_id' => '2', 'user_id' => '1', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id' => '4', 'title' => 'Book4', 'summary' => 'This is book 4.', 'category_id' => '3', 'user_id' => '2', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id' => '5', 'title' => 'Book5', 'summary' => 'This is book 5.', 'category_id' => '3', 'user_id' => '2', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
