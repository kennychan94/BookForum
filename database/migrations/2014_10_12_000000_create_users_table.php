<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('role', ['author', 'reader'])->default('reader');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            ['id'=>'1', 'name'=>'author1', 'email'=>'author1@gmail.com', 'password'=>bcrypt('author1'), 'role'=>'author', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'2', 'name'=>'author2', 'email'=>'author2@gmail.com', 'password'=>bcrypt('author2'), 'role'=>'author', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
            ['id'=>'3', 'name'=>'reader1', 'email'=>'reader1@gmail.com', 'password'=>bcrypt('reader1'), 'role'=>'reader', 'created_at' => '2018-04-21 09:45:25', 'updated_at' => '2018-04-21 09:45:25'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
