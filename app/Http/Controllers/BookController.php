<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Book;
use App\Comment;
use Auth;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        
        $this->authorize('view', Book::class);
        $books = Book::orderBy('id', 'asc')->get();
        return view('book.index', ['books' => $books]);
    }

    public function create()
    {
        $this->authorize('create', Book::class);
        $book = new Book();
        return view('book.create', [
            'book' => $book,
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'  => 'required',
            'summary'  =>  'required|max:100',
            'category_id' => 'required'
        ]);

        $book = new Book();
        $book->fill($request->all());
        $book->user_id = Auth::user()->id;
        $book->save();
    
        return redirect()->route('book.index');
    }

    public function show($id)
    {
        $this->authorize('view', Book::class);
        $book = Book::find($id);
        $comments = Comment::where('book_id', $book->id)->get();
        if(!$book) throw new ModelNotFoundException;
        if(!$comments) throw new ModelNotFoundException;

        return view('book.show')->with('book', $book)->with('comments', $comments);
    }

    public function edit($id)
    {
        $book = book::find($id);
        if(!$book) throw new ModelNotFoundException;

        $this->authorize('manage', $book);
        return view('book.edit', [
        'book' => $book
        ]);
    }

    public function update(Request $request, $id)
    {
        $book = book::find($id);
        if(!$book) throw new ModelNotFoundException;

        $book->fill($request->all());

        $book->save();

        return redirect()->route('book.index');
    }


    public function delete($id)
    {
        $book = book::find($id);
        if(!$book) throw new ModelNotFoundException;

        $this->authorize('delete', $book);
        $book->delete();

        return redirect()->route('book.index');

    }

    public function commentStore(Request $request, $id)
    {
        $this->validate($request, [
            'content'  => 'required',
        ]);
        $book = book::find($id);
        $comment = new Comment();
        $comment->fill($request->all());
        $comment->user_id = Auth::user()->id;
        $comment->book_id = $book->id;
        $comment->save();
    
        // return redirect()->route('/book/show/'.$id);
        return redirect('/book/show/'.$id );
    }
}
