<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Comment;
use App\Category;

class Book extends Model
{
    //
    protected $fillable = [
        'title',
        'summary',
        'category_id',
        'user_id',   
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function comment(){
        return $this->hasMany(Comment::class);
    }

    public function category()
	{
		return $this->belongsToMany(Category::class);
	}

}
