<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Book;

class Category extends Model
{
    //
    protected $fillable = [
        'name',  
    ];

    public function book()
	{
		return $this->belongsToMany(Book::class);
	}
}
